# Úvod do pythonu 

``` py
print("ahoj")
```

## Spuštění
``` sh
python main.py
```

## Proměnné

``` py
message = "hello"
print(message)
```

## Funkce
``` py
def say_hello():
    print("ahoj")

say_hello()
```

## Parametry
```py
def say_hello(text):
    print(text)
    
say_hello("hello")
say_hello("hi")
```

## Součet
```py
def soucet(x, y):
    return x + y

z = soucet(10, 20)
print(z)
```
## Podmínky

```py
def soucet(x, y):
    if x == 0:
        return 1000000
    return x + y

z = soucet(0, 84054)
print(z)
```
## Faktorial

```py
def factorial(x):
    if x == 1 or x == 0:
        return 1
    return x*factorial(x-1)    
z = factorial(10)
print(z)
```
## Cykly
``` py
jmena = ['pepa', 'ferda', 'jarda', ]

for jmeno in jmena:
    print(jmeno)
```

## Cykly násobky 7
``` py
for i in range(10):
    print((i+1)*7)
```
## Cykly výpis násobilky
``` py
for i in range(10):
    for y in range(11):
        print((i+1)*y)
```
## Seřazení čísel
``` py 
cisla = [5, 3, 6, 8, 7, 2, 1, 4, ]

serazeno = False
while not serazeno:
    serazeno = True
    for i in range(len(cisla) - 1):
        if cisla[i] > cisla[i+1]:
            x = cisla [i] 
            cisla [i] = cisla [i+1]
            cisla [i+1] = x 
            serazeno = False
for x in cisla:
    print(x)
```
## Tabulka násobilky
``` py
for i in range(1, 11):
    A = ''
    for j in range(1,11):
        A=A+format(i*j,'4')
    print(A)
```
## Tabulka násobilky lépe
``` py
for x in range(10):
    l = []
    for i in range(10):
        l.append((i + 1) * (x + 1))

    print(*l)
```
## append v py
```
 přidá do pole []
```
## kecací hodina
```
sítě - P2P - přímé spojení
    switch - uzlovej bod, kam se to napojuje, potom do routeru
    LAN - local area network
    WLAN - to samý bez drátu
    WAN - wide area network - port co komunikuje s vnejskem
    TCP/IP - protokol, umožní nám komunikaci
    HTTP - něco navíc
    HTTPS - navíc šifrované
    IPv4 - ip adresa
    lokální začíná na 10 nebo 192 jinak wide
    DNS - překlad tabulka názvů na ip
    DHCP - dynamické přiřazování ip adres
    MAC - fyzická adresa od výrobce
    MODEM - vysílá a přijímá, moduluje a demoduluje zvuk na bity
        optická síť to samý, ale světlo
    ROUTER - chytrý switch, ví co kam

```
## django views.py
``` py
from django.shortcuts import render
from django.http import HttpResponse

def home(request):
    return HttpResponse("Vítej v knihovně")
```
## 
```
```
##
```
```
